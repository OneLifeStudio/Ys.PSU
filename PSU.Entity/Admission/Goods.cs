﻿/* ==============================================================================
* 命名空间：PSU.Entity
* 类 名 称：SysField
* 创 建 者：Qing
* 创建时间：2018-10-02 10:14:06
* CLR 版本：4.0.30319.42000
* 保存的文件名：SysField
* 文件版本：V1.0.0.0
*
* 功能描述：物品信息表
*
* 修改历史：
*
*
* ==============================================================================
*         CopyRight @ 班纳工作室 2018. All rights reserved
* ==============================================================================*/

using PSU.Utility.System;
using System.ComponentModel.DataAnnotations;

namespace PSU.Entity.Admission
{
    public class Goods : SysField
    {
        #region Attribute

        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public string GoodsOID { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Required]
        public long Id { get; set; } = TimeUtility.GetTimespans();

        /// <summary>
        /// 物品名称
        /// </summary>
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        /// <summary>
        /// 物品描述
        /// </summary>
        [MaxLength(200)]
        public string Description { get; set; }

        /// <summary>
        /// 物品尺寸
        /// </summary>
        [MaxLength(100)]
        public string Size { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string ImageSrc { get; set; }

        /// <summary>
        /// 图片二进制流
        /// </summary>
        public byte Image { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { get; set; }

        #endregion
    }
}
