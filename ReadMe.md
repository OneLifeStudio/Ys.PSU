# ASP.NET Core 3.1 MVC项目实战

## 项目简介

* 这个项目是一个大学生在线报名系统，主要是为了完成大学生报名过程中的一些数据维护，当然最后因为时间关系，好多都没做。。。项目主要使用到的相关技术如下所示：
  * 项目框架：ASP.NET Core 3.1 MVC
  * ORM：Entity Framework Core（使用Code First）
  * 数据库引擎：SQL Server/MySQL 5.7
  * 权限验证：基于策略的权限验证(Policy-Based Authorization)
  * 前端框架：AdminLte（一个基于Bootstrap的开源前端UI）
  * 表格控件：Jquery Datatables
  * 数据可视化组件：Echarts
  * 日志记录：nlog


## 设计与实现介绍
* 项目架构采用的是多层架构，通过拆分不同的功能领域，实现各个功能间的相对独立，项目在VS中搭建完成后如下图所示。
![](./screenshots/00.png)
  * 01 Entity:实体层，包含PSU.Entity这一个系统组件，用于存储数据库中表所对应的C#对象实体。
  * 02 Infrastructure:基础架构层，包含PSU.EFCore、PSU.Utility两个系统组件。PSU.EFCore类库通过引用Entity Framework Core来完成对于数据库的操作。PSU.Utility类库中包含系统开发过程中可能用到的帮助类文件。
  * 03 Logic:逻辑层，包含PSU.Domain、PSU.Repository两个系统组件。PSU.Domain用于继承每个领域的接口类库（PSU.IService），实现领域接口中的功能。PSU.Repository用来实现PSU.Domain类库中所包含的对于数据库的操作。
  * 04 Rule:规则层，包含PSU.IService、PSU.Model这两个系统组件。PSU.IService为系统领域功能接口类库，PSU.Model为视图所对应的数据充血模型，对应MVC模式中的实体Model。
  * 05 Web:表现层
    * 1 PSU.Site:表现层，ASP.NET Core MVC项目，项目主程序。

* 系统权限验证设计：
* 整个系统分为三种角色，分别为管理员、教职工、学生用户，通过使用Area搭建每个角色的页面，通过在Controller上添加Area特性，指定当前Controller属于的角色。在创建用户时，会指定用户的角色字段，当用户登录成功后，会根据用户角色进行Claim的创建，通过自定义的AuthorizztionHandler来实现对于当前系统的角色权限控制。

## 效果预览：
![](./screenshots/01.png)
![](./screenshots/02.png)
![](./screenshots/03.png)
![](./screenshots/04.png)
![](./screenshots/05.png)

## 数据库切换：
* 首先在PSU.Site项目中的`appsettings.json`文件中添加连接字符串：
``` json
{
    "ConnectionStrings": {
        "SQLConnection": "Data Source=.;Initial Catalog=PSU;User Id=sa;Password=123456;",
        "MysqlConnection": "server=localhost;database=PSU;userid=root;pwd=123456;port=3306;sslmode=none;"
    },
  "Logging": {
    "LogLevel": {
      "Default": "Trace",
      "Microsoft": "Information"
    }
  }
}
```
* 然后在在PSU.Site项目中的`Startup.cs`文件中的`ConfigureServices`方法里配置要使用的数据库连接字符串：
``` c#
public void ConfigureServices(IServiceCollection services)
{
    // 使用SQL Server数据库 的连接
    services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SQLConnection")));
    // 使用 MySql数据库 的连接
    services.AddDbContext<ApplicationDbContext>(options => options.UseMySQL(Configuration.GetConnectionString("MySqlConnection")));
}
```
* 默认登录账户 `admin` `1234656789`

### .NET core 2.0发布后没有 views视图页面文件
* 修改PSU.Site.csproj，在PropertyGroup下加入`<MvcRazorCompileOnPublish>false</MvcRazorCompileOnPublish>`
* 下面我们开始启动我们的Web项目，使用命令行去调用`dotnet PSU.Site.dll`。
![](./screenshots/publish.png)

### 其他常见问题及解决方案
#### *.[Net Core 2.0 网站发布到Linux系统中](https://blog.csdn.net/u010584641/article/details/60329823)
#### 1.[Ubuntu16修改mysql默认字符集为utf8](https://blog.csdn.net/lxfHaHaHa/article/details/78490249)
#### 2.[EntityFrameworkCore使用CodeFirst、DBFirst数据库迁移](https://blog.csdn.net/u014690615/article/details/82718251)
#### 3.[.Net Core EntityFrameworkCore之Sqlite使用及部署](https://www.cnblogs.com/anech/p/6873385.html)
#### 4.[DotNet Core 2.0部署后外网IP访问](https://www.cnblogs.com/chenyangsocool/p/7707495.html)
#### 5.[NET Core使用Nlog记录日志](https://www.cnblogs.com/qmhuang/p/8305915.html)、[Net Core使用日志 NLog](https://blog.csdn.net/aojiancc2/article/details/74923234)